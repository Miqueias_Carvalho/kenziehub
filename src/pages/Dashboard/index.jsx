import { Container, CardContainer } from "./styles";
import { Redirect } from "react-router-dom";
import { useEffect, useState } from "react";
import api from "../../services/api";
import Card from "../../components/Cards";
import NewTechCard from "../../components/NewTechCard";

//JSX
//=======================================================================
function Dashboard({ authenticated }) {
  const user = JSON.parse(localStorage.getItem("@kenziehub:user")) || "";

  const [token] = useState(
    JSON.parse(localStorage.getItem("@kenziehub:token")) || ""
  );

  const [technologies, setTechnologies] = useState(user.techs);

  //Funções
  //----------------------------------------------------------------------------
  //carrega as techs
  function loadTechs() {
    api
      .get(`/users/${user.id}`)
      .then((response) => {
        setTechnologies(response.data.techs);
      })
      .catch((err) => console.log(err.response.data.message));
  }

  //add uma nova tech
  const newTech = (data) => {
    api
      .post(
        "/users/techs",
        { title: data.title, status: data.status },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => loadTechs())
      .catch((err) => console.log(err.response.data.message));
  };

  //remove uma tech escolhida
  const remove = (id) => {
    const filteredTechs = technologies.filter((tech) => tech.id !== id);

    api
      .delete(`/users/techs/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(() => setTechnologies(filteredTechs));
  };

  //atualizando a lista de tecnologias-----------------------------------
  useEffect(() => {
    loadTechs();
  }, []);

  //----------------------------------------------------------------------------

  if (!authenticated) {
    return <Redirect to="/" />;
  }
  //---------------------------------------------------------------------------
  //return JSX
  //=======================================================================
  return (
    <Container>
      <h1>Dashboard</h1>

      <h2>Cadastrar Tecnologia</h2>
      <NewTechCard newTech={newTech} />

      <hr></hr>
      <h2>Suas Tecnologias</h2>
      <CardContainer>
        {technologies.map((techs) => (
          <Card
            key={techs.id}
            id={techs.id}
            title={techs.title}
            status={techs.status}
            remove={remove}
          />
        ))}
      </CardContainer>
    </Container>
  );
}
export default Dashboard;
