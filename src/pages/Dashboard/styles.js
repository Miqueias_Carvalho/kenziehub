import styled from "styled-components";

export const Container = styled.div`
  background: var(--lavander-blush);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h1 {
    margin-top: 30px;
  }

  hr {
    width: 95%;
    margin: 20px 0;
    height: 5px;
    background-color: black;
  }

  h2 {
    margin: 8px 0;
    color: var(--gray);
  }
  @media (min-width: 768px) {
    height: 100vh;
  }
`;

export const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
