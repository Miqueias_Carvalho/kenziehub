import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: stretch;
  justify-content: center;
  background: var(--lavander-blush);
  flex-direction: column-reverse;

  > div {
    margin-top: 60px;
  }

  @media (min-width: 768px) {
    flex-direction: row;
    height: 100vh;

    > div + div {
      border-left: 1px solid var(--gray);
    }
  }
`;
