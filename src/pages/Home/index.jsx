import Cadastro from "../../components/Cadastro";
import Login from "../../components/Login";
import { Container } from "./styles";
import { Redirect } from "react-router-dom";

function Home({ authenticated, setAuthenticated }) {
  if (authenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Cadastro />

      <Login setAuthenticated={setAuthenticated} />
    </Container>
  );
}
export default Home;
