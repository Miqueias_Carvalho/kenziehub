import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
*{
    margin:0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;

    
}

:root{
    --white:#f5f5f5;
    --black: #0c0d0d;
    --gray: #666360;
    --red: #c53030;
    --lavander-blush:#F0E2E7
}

button{
    cursor: pointer;
}

a{
    text-decoration: none;
}
`;
