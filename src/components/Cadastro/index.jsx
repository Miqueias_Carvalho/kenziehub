import Button from "../Button";
import Input from "../Input";
import { Container } from "./styles";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import InputSelect from "../InputSelect";
import api from "../../services/api";
import { toast } from "react-toastify";

function Cadastro() {
  const schema = yup.object().shape({
    email: yup.string().required("Campo Obrigatório").email("Email inválido"),
    password: yup
      .string()
      .required("Campo Obrigatório")
      .min(6, "Mínimo 6 characteres"),
    name: yup
      .string()
      .required("Campo Obrigatório")
      .matches(/^[a-záàâãéèêíïóôõöúçñ ]+$/i, "*Apenas letras"),
    bio: yup
      .string()
      .required("Campo Obrigatório")
      .min(10, "Mínimo 10 characteres"),
    contact: yup.string().required("Campo Obrigatório"),
    // course_module: yup.string().required("Campo Obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmitFunction = (data) => {
    api
      .post("/users", data)
      .then((response) => {
        toast.success("Sucesso ao criar a conta");
      })
      .catch((err) => {
        toast.error(err.response.data.message);
      });
  };

  //return JSX
  //=================================================================
  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <h2>Cadastro</h2>
        <Input
          label="Nome"
          name="name"
          register={register}
          error={errors.name?.message}
          placeholder="digite seu Nome"
        />
        <Input
          label="Descrição"
          name="bio"
          register={register}
          error={errors.bio?.message}
          placeholder="digite a descrição"
        />
        <Input
          label="Contato"
          name="contact"
          register={register}
          error={errors.contact?.message}
          placeholder="digite forma de Contato"
        />

        <Input
          label="Email"
          name="email"
          register={register}
          error={errors.email?.message}
          placeholder="digite seu email"
        />
        <Input
          label="Senha"
          name="password"
          register={register}
          error={errors.password?.message}
          placeholder="digite sua senha"
          type="password"
        />
        <InputSelect
          label="Módulo"
          name="course_module"
          register={register}
          error={errors.couser_module?.message}
          required
        />

        <Button type="submit">Cadastrar</Button>
      </form>
    </Container>
  );
}
export default Cadastro;
