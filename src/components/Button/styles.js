import styled from "styled-components";

export const Container = styled.button`
  height: 45px;
  border-radius: 8px;
  background: var(--black);
  color: var(--white);
  font-size: 1rem;
  margin-top: 16px;
  width: 100%;
`;
