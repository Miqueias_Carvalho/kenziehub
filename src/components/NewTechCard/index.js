import { Container } from "./styles";
import Input from "../Input";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import Button from "../Button";

function NewTechCard({ newTech }) {
  const schema = yup.object().shape({
    title: yup.string().required("Campo Obrigatório"),
    status: yup.string().nullable().required("Campo Obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data) => {
    newTech(data);
  };

  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Input
          label="Tecnologia"
          name="title"
          placeholder="Digite"
          register={register}
          error={errors.tech?.message}
        />
        <div>
          <p>
            Nível - {!!errors.status && <span>{errors.status.message}</span>}
          </p>
          <input
            type="radio"
            name="status"
            id="iniciante"
            value="Iniciante"
            {...register("status")}
          />
          <label htmlFor="iniciante">Iniciante</label>

          <input
            type="radio"
            name="status"
            id="intermediario"
            value="Intermediário"
            {...register("status")}
          />
          <label htmlFor="intermediario">Intermediário</label>

          <input
            type="radio"
            name="status"
            id="avancado"
            value="Avançado"
            {...register("status")}
          />
          <label htmlFor="avancado">Avançado</label>
        </div>

        <Button type="submit">Cadastrar</Button>
      </form>
    </Container>
  );
}
export default NewTechCard;
