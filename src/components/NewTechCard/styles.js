import styled from "styled-components";

export const Container = styled.div`
  div {
    margin-top: 10px;
  }

  div {
    span {
      color: var(--red);
    }
  }
  form {
    div {
      label + input {
        margin-left: 30px;
      }
    }
  }
`;
