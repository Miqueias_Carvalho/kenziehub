import styled from "styled-components";

export const Container = styled.div`
  width: 150px;
  height: 200px;
  background: var(--white);
  margin: 5px;
  border-radius: 8px;
  border-left: 2px solid var(--gray);
  border-top: 2px solid;
  box-shadow: 4px 4px 8px var(--gray);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;

  p {
    text-align: center;
  }

  div {
    button {
      height: 25px;
      font-size: 0.8rem;
      margin-top: 10px;
      width: 100px;
    }
  }
`;
