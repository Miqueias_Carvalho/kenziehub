import Button from "../Button";
import { Container } from "./styles";

function Card({ title, status, id, remove, update }) {
  return (
    <Container>
      <h3>{title}</h3>
      <p>Nível - {status}</p>
      <div>
        <Button onClick={() => remove(id)}>Remove</Button>
      </div>
    </Container>
  );
}
export default Card;
