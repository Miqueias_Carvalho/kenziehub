import styled from "styled-components";

export const Container = styled.div`
  text-align: left;
  div {
    span {
      color: var(--red);
    }
  }
`;
export const InputContainer = styled.div`
  background: var(--white);
  border-radius: 10px;
  border: 2px solid var(--gray);
  color: var(--gray);
  padding: 0.8rem;
  width: 100%;
  display: flex;

  input {
    font-size: 1rem;
    background: transparent;
    color: var(--black);
    border: 0;
    flex: 1;

    &::placeholder {
      color: var(--gray);
    }
  }
`;
