import { Container } from "./styles";
import Button from "../Button";
import Input from "../Input";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

function Login({ setAuthenticated }) {
  //validações
  //---------------------------------------------------------------
  const schema = yup.object().shape({
    email: yup.string().required("Campo Obrigatório").email("Email inválido"),
    password: yup
      .string()
      .required("Campo Obrigatório")
      .min(6, "Mínimo 6 characteres"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const history = useHistory();

  //Funções
  //--------------------------------------------------------------------
  const onSubmitFunction = (data) => {
    console.log(data);
    api
      .post("/sessions", data)
      .then((response) => {
        console.log(response);
        toast.success("Logado com sucesso");

        const { token, user } = response.data;
        localStorage.setItem("@kenziehub:token", JSON.stringify(token));
        localStorage.setItem("@kenziehub:user", JSON.stringify(user));

        setAuthenticated(true);
        return history.push("/dashboard");
      })
      .catch((err) => {
        toast.error(err.response.data.message);
      });
  };
  //return do JSX
  //===========================================================================
  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <h2>Login</h2>
        <Input
          register={register}
          name="email"
          label="Email"
          error={errors.email?.message}
          placeholder="email"
        />
        <Input
          register={register}
          name="password"
          error={errors.password?.message}
          label="Senha"
          placeholder="password"
          type="password"
        />
        <Button type="submit">Entrar</Button>
      </form>
    </Container>
  );
}
export default Login;
