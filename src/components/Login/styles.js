import styled from "styled-components";

export const Container = styled.div`
  flex: 1;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  /* justify-content: center; */

  h2 {
    margin: 5px 0 15px 0;
  }
`;
