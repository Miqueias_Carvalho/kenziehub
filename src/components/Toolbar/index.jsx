import { Container } from "./styles";
import { GiExitDoor } from "react-icons/gi";
import { useHistory } from "react-router-dom";

function Toolbar({ setAuthenticated }) {
  const history = useHistory();

  const exit = () => {
    localStorage.clear();
    setAuthenticated(false);
    history.push("/");
  };

  return (
    <Container>
      <GiExitDoor onClick={exit} />
    </Container>
  );
}
export default Toolbar;
