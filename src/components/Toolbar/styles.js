import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 45px;
  background: var(--black);
  color: (--white);
  display: flex;
  justify-content: flex-end;
  align-items: center;

  svg {
    color: var(--white);
    margin-right: 5%;
    font-size: 1.5rem;
    cursor: pointer;
  }
`;
