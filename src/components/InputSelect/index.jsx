import { Container, InputContainer } from "./styles";

function InputSelect({ label, register, name, error = "", ...rest }) {
  return (
    <Container>
      <div>
        {label} {!!error && <span>{error}</span>}
      </div>

      <InputContainer>
        <select name={name} {...register(name)} {...rest}>
          {/* <option value="" disabled selected hidden>
            Escolha o Módulo
          </option> */}
          <option value="Primeiro módulo (Introdução ao Frontend)">
            Introdução ao Frontend
          </option>
          <option value="Segundo módulo (Frontend Avançado)">
            Frontend Avançado
          </option>
          <option value="Terceiro módulo (Introdução ao Backend)">
            Introdução ao Backend
          </option>
          <option value="Quarto módulo (Backend Avançado)">
            Backend Avançado
          </option>
        </select>
      </InputContainer>
    </Container>
  );
}
export default InputSelect;
